prompt PL/SQL Developer Export User Objects for user IFSAPP@CHEMIDEV
prompt Created by DinushadeSilva on 28 August 2020
set define off
spool Covalent_Pod_Api.log

prompt
prompt Creating package COVALENT_POD_API
prompt =================================
prompt
CREATE OR REPLACE PACKAGE COVALENT_POD_API IS


module_  CONSTANT VARCHAR2(25) := 'FNDBAS';
lu_name_ CONSTANT VARCHAR2(25) := 'CovalentPod';
pkg_     CONSTANT VARCHAR2(25) := 'COVALENT_POD_API';

-----------------------------------------------------------------------------
-------------------- LU SPECIFIC PUBLIC METHODS -----------------------------
-----------------------------------------------------------------------------

PROCEDURE Get_All_Shipments(shipment_      OUT CLOB,
                            email_address_ IN  VARCHAR2);
                            
PROCEDURE Update_Shipment(info_             OUT VARCHAR2,
                          shipment_id_      IN  NUMBER,                   
                          response_         IN  VARCHAR2,                 
                          email_address_    IN  VARCHAR2);
                                                   
PROCEDURE Update_Shipment_Line(info_             OUT VARCHAR2,
                               shipment_id_      IN  NUMBER, 
                               shipment_line_no_ IN  NUMBER, 
                               qty_received_     IN  NUMBER,                
                               response_         IN  VARCHAR2,                 
                               email_address_    IN  VARCHAR2);         
                                    
PROCEDURE Attach_Sig_To_Shipment ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  );      
                                   
PROCEDURE Attach_Pic_To_Shipment ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  );  
                                   
PROCEDURE Attach_Pic_To_Ship_Line ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN  NUMBER,
                                   shipment_line_no_           IN  NUMBER,
                                   title_                      IN  VARCHAR2,
                                   file_data_                  IN  BLOB  );    
                                   
PROCEDURE Update_Shipment_note(info_             OUT VARCHAR2,
                               shipment_id_      IN  NUMBER,                   
                               note_text_        IN  VARCHAR2);  
                               
PROCEDURE Update_Ship_line_note(info_             OUT VARCHAR2,
                                shipment_id_      IN  NUMBER, 
                                shipment_line_no_ IN  NUMBER,                  
                                note_text_        IN  VARCHAR2);     
                                
PROCEDURE Get_All_Collections(collection_      OUT CLOB,
                              email_address_   IN  VARCHAR2);  
                              
PROCEDURE Update_Collection(info_             OUT VARCHAR2,
                            rma_no_           IN  NUMBER,                   
                            response_         IN  VARCHAR2,                 
                            email_address_    IN  VARCHAR2);    
                            
PROCEDURE Update_Collection_Line(info_             OUT VARCHAR2,
                                 rma_no_           IN  NUMBER, 
                                 rma_line_no_      IN  NUMBER, 
                                 qty_collected_    IN  NUMBER,                
                                 response_         IN  VARCHAR2,                 
                                 email_address_    IN  VARCHAR2);     
                                 
PROCEDURE Attach_Pic_To_Collection(response_                  OUT VARCHAR2,
                                   rma_no_                     IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  );   
                                   
PROCEDURE Attach_Pic_To_Col_Line ( response_                  OUT VARCHAR2,
                                   rma_no_                     IN  NUMBER,
                                   rma_line_no_                IN  NUMBER,
                                   title_                      IN  VARCHAR2,
                                   file_data_                  IN  BLOB  );  
                                   
PROCEDURE Attach_Sig_To_Collection(response_                  OUT VARCHAR2,
                                   rma_no_                     IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  );
                                   
PROCEDURE Update_Collection_note(info_             OUT VARCHAR2,
                                 rma_no_           IN  NUMBER,                   
                                 note_text_        IN  VARCHAR2);
                                 
PROCEDURE Update_Col_Line_note(info_             OUT VARCHAR2,
                               rma_no_           IN  NUMBER, 
                               rma_line_no_      IN  NUMBER,                  
                               note_text_        IN  VARCHAR2);     
                               
PROCEDURE Packing_List_Email(info_                 OUT VARCHAR2,
                             shipment_id_list_     IN VARCHAR2,
                             layout_name_          IN VARCHAR2 DEFAULT NULL);    
                             
PROCEDURE Return_Note_Email(info_                 OUT VARCHAR2,
                            rma_no_list_           IN VARCHAR2,
                            layout_name_           IN VARCHAR2 DEFAULT NULL);      
                            
PROCEDURE Update_Col_Mail_Sent(info_              OUT VARCHAR2,
                               rma_no_list_        IN VARCHAR2);   
                               
PROCEDURE Update_Ship_Mail_Sent(info_                 OUT VARCHAR2,
                                shipment_id_list_      IN VARCHAR2);    
                                
PROCEDURE Get_Next_Stop_Shipments (shipment_       OUT CLOB,
                                   email_address_   IN VARCHAR2);    

PROCEDURE Get_Next_Stop_Collections(rma_             OUT CLOB,
                                    email_address_   IN  VARCHAR2);  
                                    
PROCEDURE Get_Next_Stop_Items(item_             OUT CLOB,
                              email_address_    IN  VARCHAR2);                                    
                                    
PROCEDURE Update_txn_status (info_             OUT VARCHAR2,
                             source_           IN  VARCHAR2, 
                             source_id_        IN  NUMBER,                  
                             transport_id_     IN  VARCHAR2,
                             status_           IN  VARCHAR2);  
                             
FUNCTION Get_Planned_Ship_Date(source_         IN  VARCHAR2,
                               source_id_      IN  NUMBER)RETURN DATE;     
                               
FUNCTION Get_Customer_Name (source_         IN  VARCHAR2,
                            source_id_      IN  NUMBER) RETURN VARCHAR2; 
                               
FUNCTION Get_Address (source_         IN  VARCHAR2,
                      source_id_      IN  NUMBER) RETURN VARCHAR2;
                      
FUNCTION Get_Zip_Code (source_         IN  VARCHAR2,
                       source_id_      IN  NUMBER) RETURN VARCHAR2;                      
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
-----------------------------------------------------------------------------
-------------------- FOUNDATION1 METHODS ------------------------------------
-----------------------------------------------------------------------------

PROCEDURE Init;

END COVALENT_POD_API;
/


prompt Done
spool off
set define on
