prompt PL/SQL Developer Export User Objects for user IFSAPP@CHEMIDEV
prompt Created by DinushadeSilva on 28 August 2020
set define off
spool Covalent_Pod_Apy.log

prompt
prompt Creating package body COVALENT_POD_API
prompt ======================================
prompt
CREATE OR REPLACE PACKAGE BODY COVALENT_POD_API IS



-----------------------------------------------------------------------------
-------------------- LU SPECIFIC IMPLEMENTATION METHOD DECLARATIONS ---------
-----------------------------------------------------------------------------

PROCEDURE Send_Email(result_key_string_   IN VARCHAR2,
                     customer_name_       IN VARCHAR2,
                     company_email_       IN VARCHAR2,
                     company_name_        IN VARCHAR2,
                     address_             IN VARCHAR2,
                     cust_ord_no_string_  IN VARCHAR2,
                     cust_po_no_string_   IN VARCHAR2,
                     to_email_address_    IN VARCHAR2,
                     source_              IN VARCHAR2,
                     pdf_count_           IN NUMBER);

-----------------------------------------------------------------------------
-------------------- LU SPECIFIC IMPLEMENTATION METHODS ---------
-----------------------------------------------------------------------------

PROCEDURE Send_Email(result_key_string_   IN VARCHAR2,
                     customer_name_       IN VARCHAR2,
                     company_email_       IN VARCHAR2,
                     company_name_        IN VARCHAR2,
                     address_             IN VARCHAR2,
                     cust_ord_no_string_  IN VARCHAR2,
                     cust_po_no_string_   IN VARCHAR2,
                     to_email_address_    IN VARCHAR2,
                     source_              IN VARCHAR2,
                     pdf_count_           IN NUMBER) IS
  
  v_blob      BLOB;
  v_length    NUMBER;
  v_offset    NUMBER := 1;
  v_chunk     NUMBER := 32767;
  
  l_buffer    RAW(32767);
  l_amount    BINARY_INTEGER := 32767;
  v_output    UTL_FILE.file_type;
  
  v_ora_dir_  VARCHAR2(100) := 'IFS-DATAMIG'; -- Oracle directory
  
  path_dir_   VARCHAR2(100) := '\\10.0.0.4\IFS_Media\DATAMIG'; -- Should have same path as the oracle directory
  
  file_list_      VARCHAR2(32000);
  mail_subject_   VARCHAR2(32000);
  message_        VARCHAR2(32000);
  message1_       VARCHAR2(32000);
  message2_       VARCHAR2(32000);
  message3_       VARCHAR2(32000);
  start_position_ NUMBER:=0;

  CURSOR Get_quotation_data IS
  SELECT A.PDF,TRIM (SUBSTR(B.NOTES, start_position_)) AS "NOTES"
    FROM PDF_ARCHIVE_TAB A
    JOIN ARCHIVE_TAB B
      ON A.RESULT_KEY = B.RESULT_KEY
  WHERE A.RESULT_KEY IN(SELECT regexp_substr(result_key_string_,'[^,]+', 1, level) shipment_id FROM dual 
                        CONNECT BY regexp_substr(result_key_string_, '[^,]+', 1, level) IS NOT NULL);
  
BEGIN
 General_SYS.Init_Method(lu_name_, 'COVALENT_POD_API', 'Send_Email');
 
 IF source_ = 'SHIPMENT' THEN
   start_position_ := 14;
   mail_subject_ := 'Arrival Alert & Delivery Details';
 ELSIF source_ = 'RMA' THEN
   start_position_ := 35;
   mail_subject_ := 'Arrival Alert & Collection Details';
 END IF;
 
 IF pdf_count_ <= 10 THEN  -- if the number of reports are greater than 10, we don't attach them in email
  
   FOR quote_rec_ IN Get_quotation_data LOOP
    
      v_blob   := quote_rec_.PDF;
      v_length := DBMS_LOB.getlength(v_blob);
      v_offset := 1;
      v_chunk  := 32767;
      
   
      v_output := UTL_FILE.fopen(v_ora_dir_,TRIM(quote_rec_.NOTES) ||'.pdf','wb',32767);
      
      WHILE v_offset < v_length LOOP
        
        DBMS_LOB.read(v_blob, l_amount, v_offset, l_buffer);
        UTL_FILE.put_raw(v_output, l_buffer, TRUE);
        v_offset := v_offset + l_amount;
        
      END LOOP;
      
      UTL_FILE.fclose (v_output);
      
      file_list_ := file_list_ || ',' || path_dir_|| '\' || quote_rec_.NOTES ||'.pdf';
      
   END LOOP;
 
 ELSE
   file_list_ := NULL;
 END IF;

 dbms_output.put_line(file_list_);
  
 message1_ := '<!DOCTYPE html><html><head><title>Page Title</title></head><body><p>Dear ' || customer_name_ || ',<br><br>Please note that our driver is approximately 30 minutes from arrival to your premises. Please see attached distribution documents for the details below.<br><br>'; 
      
 message2_ := 'Our Order Details: ' || SUBSTR(cust_ord_no_string_, 2) || '<br>Your Order Details: ' || SUBSTR(cust_po_no_string_, 2) 
  || '<br>Address Details: ' || address_ || '<br><br>';
      
 message3_ := 'If you would like us to change the e-mail address that these notifications are sent to please send an e-mail to ' || company_email_ || ' stating which e-mail address is to be used in future.<br><br>Kind regards,<br><br>' || company_name_ || '</p></body></html>';
      
 message_ := message1_ || message2_ || message3_;
    
 command_sys.Mail('IFSAPP',to_email_address_,message_,attach_ => file_list_,subject_ => mail_subject_);

 COMMIT;
  
END Send_Email;

-----------------------------------------------------------------------------
-------------------- LU SPECIFIC PUBLIC METHOD IMPLEMENTATION ---------
-----------------------------------------------------------------------------

PROCEDURE Get_All_Shipments(shipment_      OUT CLOB,
                            email_address_ IN  VARCHAR2) IS
  
 json_shipment_             pljson;
 json_single_shipment_      pljson;
 json_delivey_addr_         pljson;
 json_shipment_line_        pljson;
 
 json_ar_shipment_          pljson_list;
 json_ar_ship_line_         pljson_list;
 
 json_                      CLOB;
 cust_email_                VARCHAR2(1000);
 
 CURSOR get_shipment_info IS
   SELECT t.shipment_id, t.ship_inventory_location_no, t.objstate, t.planned_ship_date, t.consignment_note_id,
   t.route_id, t.receiver_id, t.receiver_type_db, t.receiver_addr_id, t.receiver_address1, t.receiver_address2, t.receiver_address3,
   t.receiver_city, t.receiver_county, t.receiver_zip_code, t.cf$_delivery_status
   FROM shipment_cfv t
   WHERE t.objstate IN ('Preliminary','Completed')
   AND SUBSTR(t.cf$_Delivery_App_User, 1, INSTR(t.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                     FROM fnd_user_property_tab t
                                                                                     WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                     AND t.value = email_address_);
   --AND shipment_id IN (8326);     
   
 CURSOR get_ship_line(ship_id_   NUMBER) IS
   SELECT t.source_ref1,t.shipment_line_no, t.source_part_no, t.source_part_description, t.qty_picked 
   FROM shipment_line_tab t
   WHERE t.shipment_id = ship_id_;
   
 CURSOR get_cust_email(cust_id_ VARCHAR2) IS
  SELECT t.value 
  FROM COMM_METHOD_TAB t
  WHERE t.party_type = 'CUSTOMER' 
  AND t.identity = cust_id_
  AND t.name = 'Delivery Note Email'
  AND t.method_id = 'E_MAIL';  
 
  
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Get_All_Shipments');
  
  json_shipment_            := pljson();
  json_single_shipment_     := pljson();
  json_delivey_addr_        := pljson();
  json_shipment_line_       := pljson();

  json_ar_shipment_         :=  pljson_list();
  json_ar_ship_line_        :=  pljson_list();
  
  
  FOR shipment_rec_ IN get_shipment_info LOOP
    
    cust_email_             := NULL;
    
    OPEN get_cust_email(shipment_rec_.receiver_id);
    FETCH get_cust_email INTO cust_email_;
    CLOSE get_cust_email;
  
    json_single_shipment_.put ('ShipmentLocationNo'         , shipment_rec_.ship_inventory_location_no);
    json_single_shipment_.put ('Consignment'                , shipment_rec_.consignment_note_id);
    json_single_shipment_.put ('RouteDescription'           , Delivery_Route_API.Get_Description(shipment_rec_.route_id));
    json_single_shipment_.put ('CustomerNo'                 , shipment_rec_.receiver_id);
    json_single_shipment_.put ('CustomerName'               , Shipment_Source_Utility_API.Get_Receiver_Name(shipment_rec_.receiver_id,shipment_rec_.receiver_type_db));
    json_single_shipment_.put ('ContactNo'                  , Customer_Info_Address_Api.Get_Primary_Contact(shipment_rec_.receiver_id, Shipment_API.Get_Receiver_Addr_Id(shipment_rec_.shipment_id)));
    json_single_shipment_.put ('CustomerEmail'              , cust_email_);
    
    json_delivey_addr_.put ('ReceiverAddrId'              , shipment_rec_.receiver_addr_id);
    json_delivey_addr_.put ('ReceiverAddr1'               , shipment_rec_.receiver_address1);
    json_delivey_addr_.put ('ReceiverAddr2'               , shipment_rec_.receiver_address2);
    json_delivey_addr_.put ('ReceiverAddr3'               , shipment_rec_.receiver_address3);
    json_delivey_addr_.put ('ReceiverCity'                , shipment_rec_.receiver_city);
    json_delivey_addr_.put ('ReceiverCounty'              , shipment_rec_.receiver_county);
    json_delivey_addr_.put ('ReceiverZipCode'             , shipment_rec_.receiver_zip_code);
    
    
    json_single_shipment_.put ('DeliveryAddress'         , json_delivey_addr_);
    json_single_shipment_.put ('DeliveryDate'            , TO_CHAR((shipment_rec_.planned_ship_date), 'DD/MM/YYYY HH24:MI:SS'));
    json_single_shipment_.put ('ShipmentId'              , shipment_rec_.shipment_id);
    json_single_shipment_.put ('ShipmentState'           , shipment_rec_.objstate);
    json_single_shipment_.put ('DeliveryStatus'          , shipment_rec_.cf$_delivery_status);
    
    json_ar_ship_line_         :=  pljson_list();
    
    FOR line_rec_ IN get_ship_line(shipment_rec_.shipment_id) LOOP
      json_shipment_line_.put ('OrderNo'         , line_rec_.source_ref1);
      json_shipment_line_.put ('LineNo'          , line_rec_.shipment_line_no);
      json_shipment_line_.put ('PartNo'          , line_rec_.source_part_no);
      json_shipment_line_.put ('PartDescription' , line_rec_.source_part_description);
      json_shipment_line_.put ('Qty'             , line_rec_.qty_picked);
      
      json_ar_ship_line_.append (json_shipment_line_.to_json_value);
    END LOOP;
       
    json_single_shipment_.put ('ShipmentLine'      , json_ar_ship_line_);
    json_ar_shipment_.append (json_single_shipment_.to_json_value);
  
  END LOOP;
  

  json_shipment_.put ('Shipments', json_ar_shipment_);
  
  json_shipment_.print;
  
  
  
  dbms_lob.createtemporary(json_, true);
  json_shipment_.to_clob(json_, false, dbms_lob.lobmaxsize);

 /*dbms_output.put_line (dbms_lob.getlength(json_));
 
 dbms_output.put_line('PRINT OUTPUT'); -- Dinusha
 
 dbms_output.put_line(json_); -- Dinusha*/

 shipment_ := json_;

 EXCEPTION WHEN OTHERS THEN
  shipment_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);

  
END Get_All_Shipments;

PROCEDURE Update_Shipment(info_             OUT VARCHAR2,
                          shipment_id_      IN  NUMBER,                   
                          response_         IN  VARCHAR2,                 
                          email_address_    IN  VARCHAR2) IS
                          
  attr1_           VARCHAR2(32000);      
  shipment_type_   VARCHAR2(1000);
  line_info_       VARCHAR2(32000);
    
  CURSOR get_objid IS
    SELECT objid
    FROM SHIPMENT
    WHERE shipment_id = shipment_id_; 
    
  CURSOR get_shipment_lines IS
    SELECT t.shipment_line_no 
    FROM shipment_line_tab t
    WHERE t.shipment_id = shipment_id_; 
      
   
   -- p0 -> __lsResult
  p0_ VARCHAR2(32000) := NULL;

  -- p1 -> __sObjid
  objid_      VARCHAR2(32000);

  -- p2 -> __g_Bind.s[0]
  p2_ VARCHAR2(32000);

  -- p3 -> __lsAttr
  p3_ VARCHAR2(32000) := '';

  -- p4 -> __sAction
  p4_ VARCHAR2(32000) := 'DO';            
                          
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Shipment');
  
  IF (response_ = 'Shipment Received In Full') THEN
    
    IF (Shipment_Flow_API.Get_Next_Step(shipment_id_) = 'Complete') THEN
      shipment_type_ := Shipment_Api.Get_Shipment_Type(shipment_id_);
      attr1_  := 'START_EVENT'||chr(31)||'50'||chr(30)||'SHIPMENT_ID'||chr(31)|| shipment_id_ ||chr(30)||'SHIPMENT_TYPE'||chr(31)|| shipment_type_ ||chr(30)||'LOCATION_NO'||chr(31)||''||chr(30)||'END'||chr(31)||''||chr(30);
      Shipment_Flow_API.Start_Complete_Shipment__(attr1_ );
    END IF;
    
    FOR rec_ IN get_shipment_lines LOOP
      
      Update_Shipment_Line (info_             => line_info_,
                            shipment_id_      => shipment_id_,
                            shipment_line_no_ => rec_.shipment_line_no,
                            qty_received_     => 0,
                            response_         => 'FULL',
                            email_address_    => email_address_);
      
    END LOOP;
    
  ELSIF response_ = 'Refused Shipment' THEN  
    
    FOR rec_ IN get_shipment_lines LOOP
      
      Update_Shipment_Line (info_             => line_info_,
                            shipment_id_      => shipment_id_,
                            shipment_line_no_ => rec_.shipment_line_no,
                            qty_received_     => 0,
                            response_         => 'REFUSED',
                            email_address_    => email_address_);
      
    END LOOP;
    
  ELSIF response_ = 'Part Received Shipment' THEN
    null;
  ELSE
    null;  
  END IF;  
  
  -- Update the status (start)
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  p2_  := 'CF$_DELIVERY_STATUS'||chr(31)|| response_ ||chr(30);

  SHIPMENT_CFP.Cf_Modify__(p0_ , objid_ , p2_ , p3_ , p4_ );
  
  COMMIT;  
  -- Update the status (end)
  
  info_ := 'No Errors Found';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
 
END Update_Shipment;       

PROCEDURE Update_Shipment_Line(info_             OUT VARCHAR2,
                               shipment_id_      IN  NUMBER, 
                               shipment_line_no_ IN  NUMBER, 
                               qty_received_     IN  NUMBER,                
                               response_         IN  VARCHAR2,                 
                               email_address_    IN  VARCHAR2) IS
                               
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000); 
 qty_picked_        NUMBER;  
 
 CURSOR get_objid IS
    SELECT objid, qty_picked
    FROM SHIPMENT_LINE t
    WHERE shipment_id = shipment_id_
    AND shipment_line_no = shipment_line_no_;                            
                               
BEGIN
 General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Shipment_Line');
 
  OPEN get_objid;
  FETCH get_objid INTO objid_, qty_picked_;
  CLOSE get_objid;
  
  IF response_ IN ('FULL','PARTIAL','REFUSED') THEN
  
    Client_Sys.Clear_Attr(attr_cf_);
    
    IF response_ = 'PARTIAL' THEN
       Client_Sys.Add_To_Attr ('CF$_PARTIAL_RECEIVED_DB'     , 'YES'             ,attr_cf_);
    END IF;
    
    IF response_ = 'FULL' THEN
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_RECEIVED'       , qty_picked_       ,attr_cf_);
    ELSIF response_ = 'PARTIAL' THEN
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_RECEIVED'       , qty_received_     ,attr_cf_);
    ELSE -- REFUSED
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_RECEIVED'       , 0                 ,attr_cf_); 
    END IF;
    
    SHIPMENT_LINE_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
    
    COMMIT;
    
    info_ := 'Line level update performed without errors';
  
  ELSE  
     info_ := 'No line level update performed';
  END IF;
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);

END Update_Shipment_Line;   
   

PROCEDURE Attach_Sig_To_Shipment ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM shipment_tab t
    WHERE t.shipment_id = shipment_id_;
     

BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Sig_To_Shipment');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Signature',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Signature - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                              attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                              attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'Shipment'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                     
 -- insert into cov_dins_temp values (shipment_id_,title_,file_data_);    -- testing                               
                                     
commit;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Sig_To_Shipment;     

PROCEDURE Attach_Pic_To_Shipment ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM shipment_tab t
    WHERE t.shipment_id = shipment_id_;
     

BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Pic_To_Shipment');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Photo Proof',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Photo Proof - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                                attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                                attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'Shipment'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                     
 -- insert into cov_dins_temp values (shipment_id_,title_,file_data_);    -- testing                               
                                     
commit;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Pic_To_Shipment;                                         

PROCEDURE Attach_Pic_To_Ship_Line ( response_                  OUT VARCHAR2,
                                   shipment_id_                IN  NUMBER,
                                   shipment_line_no_           IN  NUMBER,
                                   title_                      IN  VARCHAR2,
                                   file_data_                  IN  BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM shipment_line_tab t
    WHERE t.shipment_id = shipment_id_
    AND t.shipment_line_no = shipment_line_no_;
     

BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Pic_To_Ship_Line');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Photo Proof',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Photo Proof - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                                attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                                attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'ShipmentLine'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                                                   
                                     
COMMIT;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Pic_To_Ship_Line;

PROCEDURE Update_Shipment_note(info_             OUT VARCHAR2,
                               shipment_id_      IN  NUMBER,                   
                               note_text_        IN  VARCHAR2) IS
                               
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000); 
                              
 CURSOR get_objid IS
    SELECT objid
    FROM SHIPMENT
    WHERE shipment_id = shipment_id_;                              
                               
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Shipment_note');
  
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  Client_Sys.Clear_Attr(attr_cf_);  
  Client_Sys.Add_To_Attr ('CF$_APP_NOTES'     , note_text_     ,attr_cf_);

  SHIPMENT_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
  COMMIT;  
  
  info_ := 'Shipment note updated';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Update_Shipment_note;                                    
                               
PROCEDURE Update_Ship_line_note(info_             OUT VARCHAR2,
                                shipment_id_      IN  NUMBER, 
                                shipment_line_no_ IN  NUMBER,                  
                                note_text_        IN  VARCHAR2) IS
                                
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000);  
 
 CURSOR get_objid IS
    SELECT objid
    FROM SHIPMENT_LINE t
    WHERE shipment_id = shipment_id_
    AND shipment_line_no = shipment_line_no_;                               
                                
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Ship_line_note');
  
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  Client_Sys.Clear_Attr(attr_cf_);  
  Client_Sys.Add_To_Attr ('CF$_APP_LINE_NOTES'     , note_text_     ,attr_cf_);
  
  SHIPMENT_LINE_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
  COMMIT;  
  
  info_ := 'Shipment line note updated';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
  
END Update_Ship_line_note;  

PROCEDURE Get_All_Collections(collection_      OUT CLOB,
                              email_address_   IN  VARCHAR2) IS
                              
 json_collection_             pljson;
 json_single_collection_      pljson;
 json_collection_addr_        pljson;
 json_collection_line_        pljson;
 
 json_ar_collection_          pljson_list;
 json_ar_collection_line_     pljson_list;
 
 json_                      CLOB;                            
                              
  CURSOR get_collection_info IS 
    SELECT t.rma_no, t.customer_no, CUST_ORD_CUSTOMER_API.Get_Name(t.CUSTOMER_NO) Customer_Name, t.ship_addr_no, t.state, t.CF$_PLAN_SHIP_DATE,
    DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_Address1(RETURN_FROM_CUSTOMER_NO,
                                                                   SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_Address1(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_Address1(ORDER_NO)) ship_address1,
       DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_Address2(RETURN_FROM_CUSTOMER_NO,
                                                                   SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_Address2(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_Address2(ORDER_NO)) ship_address2,
       DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_Address3(RETURN_FROM_CUSTOMER_NO,
                                                                   SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_Address3(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_Address3(ORDER_NO)) ship_address3,
       DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_Zip_Code(RETURN_FROM_CUSTOMER_NO,
                                                                   SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_Zip_Code(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_Zip_Code(ORDER_NO)) zip_code,
       DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_City(RETURN_FROM_CUSTOMER_NO,
                                                               SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_City(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_City(ORDER_NO)) city,
       DECODE(ORDER_NO,
              NULL,
              DECODE(SHIPMENT_ID,
                     NULL,
                     IFSAPP.Customer_Info_Address_API.Get_County(RETURN_FROM_CUSTOMER_NO,
                                                                 SHIP_ADDR_NO),
                     IFSAPP.Shipment_API.Get_Receiver_County(SHIPMENT_ID)),
              IFSAPP.Customer_Order_Address_API.Get_County(ORDER_NO)) county,
       CF$_COLLECTION_STATUS        
    FROM RETURN_MATERIAL_CFV t
    WHERE t.state IN ('Released','PartiallyReceived')
    AND SUBSTR(t.cf$_Delivery_App_User, 1, INSTR(t.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                         FROM fnd_user_property_tab t
                                                                                         WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                         AND t.value = email_address_);  
                                                                                         
  CURSOR get_collection_line(rma_no_   NUMBER) IS
    SELECT t.rma_line_no, t.catalog_no, t.catalog_desc, t.qty_to_return
    FROM RETURN_MATERIAL_LINE_TAB t
    WHERE t.rma_no = rma_no_;                                                                                     
                                                                                                                  
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Get_All_Collections');
  
  json_collection_            := pljson();
  json_single_collection_     := pljson();
  json_collection_addr_       := pljson();
  json_collection_line_       := pljson();

  json_ar_collection_         :=  pljson_list();
  json_ar_collection_line_    :=  pljson_list();
  
  
  FOR collect_rec_ IN get_collection_info LOOP
  
    json_single_collection_.put ('RmaNo'                   , collect_rec_.rma_no);
    json_single_collection_.put ('CustomerNo'              , collect_rec_.customer_no);
    json_single_collection_.put ('CustomerName'            , collect_rec_.Customer_Name);
    json_single_collection_.put ('ContactNo'               , Customer_Info_Address_Api.Get_Primary_Contact(collect_rec_.customer_no, collect_rec_.ship_addr_no));
    
    json_collection_addr_.put ('ShipAddrId'                , collect_rec_.ship_addr_no);
    json_collection_addr_.put ('ShipAddr1'                 , collect_rec_.ship_address1);
    json_collection_addr_.put ('ShipAddr2'                 , collect_rec_.ship_address2);
    json_collection_addr_.put ('ShipAddr3'                 , collect_rec_.ship_address3);
    json_collection_addr_.put ('ShipCity'                  , collect_rec_.city);
    json_collection_addr_.put ('ShipCounty'                , collect_rec_.county);
    json_collection_addr_.put ('ShipZipCode'               , collect_rec_.zip_code);
       
    json_single_collection_.put ('CollectionAddress'       , json_collection_addr_);
    json_single_collection_.put ('CollectionDate'          , TO_CHAR((collect_rec_.CF$_PLAN_SHIP_DATE), 'DD/MM/YYYY HH24:MI:SS'));
    json_single_collection_.put ('Status'                  , collect_rec_.state);
    json_single_collection_.put ('CollectionStatus'        , collect_rec_.CF$_COLLECTION_STATUS);
    
    json_ar_collection_line_         :=  pljson_list();
    
    FOR line_rec_ IN get_collection_line(collect_rec_.rma_no) LOOP
      json_collection_line_.put ('RmaLineNo'       , line_rec_.rma_line_no);
      json_collection_line_.put ('SalesPartNo'     , line_rec_.catalog_no);
      json_collection_line_.put ('PartDescription' , line_rec_.catalog_desc);
      json_collection_line_.put ('QtyToReturn'     , line_rec_.qty_to_return);
                 
      json_ar_collection_line_.append (json_collection_line_.to_json_value);
    END LOOP;
       
    json_single_collection_.put ('CollectionLine'      , json_ar_collection_line_);
    json_ar_collection_.append (json_single_collection_.to_json_value);
  
  END LOOP;
  

  json_collection_.put ('Collections', json_ar_collection_);
  
  json_collection_.print;
   
  dbms_lob.createtemporary(json_, true);
  json_collection_.to_clob(json_, false, dbms_lob.lobmaxsize);

  collection_ := json_;

EXCEPTION WHEN OTHERS THEN
  collection_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
  
END Get_All_Collections;    

PROCEDURE Update_Collection(info_             OUT VARCHAR2,
                            rma_no_           IN  NUMBER,                   
                            response_         IN  VARCHAR2,                 
                            email_address_    IN  VARCHAR2) IS
                               
  line_info_       VARCHAR2(32000);
  objid_           VARCHAR2(32000);
  attr_cf_         VARCHAR2(32000);
  attr_            VARCHAR2(32000);
    
  CURSOR get_objid IS
    SELECT objid
    FROM RETURN_MATERIAL
    WHERE rma_no = rma_no_; 
    
  CURSOR get_rma_lines IS
    SELECT t.rma_line_no 
    FROM RETURN_MATERIAL_LINE t
    WHERE t.rma_no = rma_no_;            
                          
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Collection');
  
  IF (response_ = 'Collection Completed') THEN   
    
    FOR rec_ IN get_rma_lines LOOP
                            
      Update_Collection_Line(info_          => line_info_,
                             rma_no_        => rma_no_,
                             rma_line_no_   => rec_.rma_line_no,
                             qty_collected_ => 0,
                             response_      => 'FULL',
                             email_address_ => email_address_);                      
      
    END LOOP;
    
  ELSIF response_ = 'Failed Collection' THEN  
    
    FOR rec_ IN get_rma_lines LOOP
       
      Update_Collection_Line(info_          => line_info_,
                             rma_no_        => rma_no_,
                             rma_line_no_   => rec_.rma_line_no,
                             qty_collected_ => 0,
                             response_      => 'REFUSED',
                             email_address_ => email_address_);                     
                            
    END LOOP;
    
  ELSIF response_ = 'Part Collection' THEN
    null;
  ELSE
    null;  
  END IF;  
  
  -- Update the status (start)
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  Client_Sys.Clear_Attr(attr_cf_);
 
  Client_Sys.Add_To_Attr ('CF$_COLLECTION_STATUS'     , response_             ,attr_cf_);

  RETURN_MATERIAL_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
  COMMIT;  
  -- Update the status (end)
  
  info_ := 'No Errors Found';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
 
END Update_Collection; 

PROCEDURE Update_Collection_Line(info_             OUT VARCHAR2,
                                 rma_no_           IN  NUMBER, 
                                 rma_line_no_      IN  NUMBER, 
                                 qty_collected_    IN  NUMBER,                
                                 response_         IN  VARCHAR2,                 
                                 email_address_    IN  VARCHAR2) IS
                               
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000); 
 qty_to_return_     NUMBER;  
 
 CURSOR get_objid IS
    SELECT objid, qty_to_return
    FROM RETURN_MATERIAL_LINE t
    WHERE rma_no = rma_no_
    AND rma_line_no = rma_line_no_;                            
                               
BEGIN
 General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Collection_Line');
 
  OPEN get_objid;
  FETCH get_objid INTO objid_, qty_to_return_;
  CLOSE get_objid;
  
  IF response_ IN ('FULL','PARTIAL','REFUSED') THEN
  
    Client_Sys.Clear_Attr(attr_cf_);
    
    IF response_ = 'PARTIAL' THEN
       Client_Sys.Add_To_Attr ('CF$_PARTIALLY_COLLECTED_DB'   , 'YES'             ,attr_cf_);
    END IF;
    
    IF response_ = 'FULL' THEN
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_COLLECTED'       , qty_to_return_    ,attr_cf_);
    ELSIF response_ = 'PARTIAL' THEN
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_COLLECTED'       , qty_collected_     ,attr_cf_);
    ELSE -- REFUSED
       Client_Sys.Add_To_Attr ('CF$_QUANTITY_COLLECTED'       , 0                 ,attr_cf_); 
    END IF;
    
    RETURN_MATERIAL_LINE_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
    
    COMMIT;
    
    info_ := 'Line level update performed without errors';
  
  ELSE  
     info_ := 'No line level update performed';
  END IF;
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);

END Update_Collection_Line;            

PROCEDURE Attach_Pic_To_Collection(response_                  OUT VARCHAR2,
                                   rma_no_                     IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM return_material_tab t
    WHERE t.rma_no = rma_no_;
     

BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Pic_To_Collection');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Photo Proof',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Photo Proof - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                                attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                                attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'ReturnMaterial'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                                                                                      
commit;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Pic_To_Collection; 

PROCEDURE Attach_Pic_To_Col_Line ( response_                  OUT VARCHAR2,
                                   rma_no_                     IN  NUMBER,
                                   rma_line_no_                IN  NUMBER,
                                   title_                      IN  VARCHAR2,
                                   file_data_                  IN  BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM return_material_line_tab t
    WHERE t.rma_no = rma_no_
    AND t.rma_line_no = rma_line_no_;
     

BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Pic_To_Col_Line');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Photo Proof',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Photo Proof - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                                attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                                attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'ReturnMaterialLine'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                                                   
                                     
COMMIT;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Pic_To_Col_Line;       

PROCEDURE Attach_Sig_To_Collection(response_                  OUT VARCHAR2,
                                   rma_no_                     IN NUMBER,
                                   title_                      IN VARCHAR2,
                                   file_data_                  IN BLOB  ) IS
                                   
 attr_                  VARCHAR2(32000);
 media_attr_            VARCHAR2(32000);
 library_item_attr_     VARCHAR2(32000);
 info_                  VARCHAR2(32000);
 objid_                 VARCHAR2(32000);
 objversion_            VARCHAR2(32000);
 item_objid_                 VARCHAR2(32000);
 item_objversion_            VARCHAR2(32000);
 item_id_               NUMBER;
 library_id_            VARCHAR2(2000);
       
CURSOR get_version IS
    SELECT rowid, ltrim(lpad(to_char(rowversion,'YYYYMMDDHH24MISS'),2000))
    FROM return_material_tab t
    WHERE t.rma_no = rma_no_;
     
BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Attach_Sig_To_Collection');

 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('NAME',                 'Signature',                          attr_);
 Client_SYS.Add_To_Attr('DESCRIPTION',          'Signature - '||title_,               attr_);
 Client_SYS.Add_To_Attr('OBSOLETE',             'FALSE',                              attr_);
 Client_SYS.Add_To_Attr('MEDIA_ITEM_TYPE',      'Image',                              attr_);

  Media_Item_Api.New__(info_ ,
                       objid_ ,
                       objversion_ ,
                       attr_,
                       'DO' );
  item_id_ := Client_Sys.Get_Item_Value('ITEM_ID',  attr_);

  Media_Item_Api.Write_Media_Object(objversion_,
                                    objid_,
                                    file_data_);


  Media_Item_Api.write_media_thumb(objversion_ ,
                                   objid_,
                                   file_data_);
 media_attr_ := attr_;
 Client_SYS.Clear_Attr(attr_);
 Client_SYS.Add_To_Attr('MEDIA_FILE',      'title_'||'.png',            attr_);

 Media_Item_Api.Modify__(info_ ,
                         objid_ ,
                         objversion_ ,
                         attr_,
                         'DO' );


  OPEN get_version;
  FETCH get_version INTO item_objid_, item_objversion_;
  CLOSE get_version;

  Media_Library_Api.Check_And_Create_Connection ( library_id_             ,
                                                  'ReturnMaterial'   ,
                                                  item_objid_);

 Client_SYS.Clear_Attr(library_item_attr_);
 Client_SYS.Add_To_Attr('ITEM_ID',           item_id_,               library_item_attr_);
 Client_SYS.Add_To_Attr('DEFAULT_MEDIA',     'FALSE',                library_item_attr_);
 Client_SYS.Add_To_Attr('NOTE_TEXT',         '',                     library_item_attr_);
 Client_SYS.Add_To_Attr('MEDIA_PRINT_OPTION',   Media_Print_Option_API.Decode('DO_NOT_PRINT'), library_item_attr_ );

 info_ := NULL;
 objid_ := NULL;
 objversion_ := NULL;

 Media_Library_Item_API.Handle_Save (info_               ,
                                     objid_              ,
                                     objversion_         ,
                                     media_attr_         ,
                                     library_item_attr_  ,
                                     library_id_         );
                                                                                                       
commit;                                     

EXCEPTION WHEN OTHERS THEN
 response_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);


END Attach_Sig_To_Collection;    

PROCEDURE Update_Collection_note(info_             OUT VARCHAR2,
                                 rma_no_           IN  NUMBER,                   
                                 note_text_        IN  VARCHAR2) IS
                               
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000); 
                              
 CURSOR get_objid IS
    SELECT objid
    FROM RETURN_MATERIAL
    WHERE rma_no = rma_no_;                              
                               
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Collection_note');
  
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  Client_Sys.Clear_Attr(attr_cf_);  
  Client_Sys.Add_To_Attr ('CF$_APP_NOTES'     , note_text_     ,attr_cf_);

  RETURN_MATERIAL_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
  COMMIT;  
  
  info_ := 'Collection note updated';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Update_Collection_note;                                  
                               
PROCEDURE Update_Col_Line_note(info_             OUT VARCHAR2,
                               rma_no_           IN  NUMBER, 
                               rma_line_no_      IN  NUMBER,                  
                               note_text_        IN  VARCHAR2) IS
                               
 objid_             VARCHAR2(2000);
 attr_cf_           VARCHAR2(2000);
 attr_              VARCHAR2(32000); 
                              
 CURSOR get_objid IS
    SELECT objid
    FROM RETURN_MATERIAL_LINE
    WHERE rma_no = rma_no_ 
    AND rma_line_no = rma_line_no_;                            
                               
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Col_Line_note');
  
  OPEN get_objid;
  FETCH get_objid INTO objid_;
  CLOSE get_objid;
  
  Client_Sys.Clear_Attr(attr_cf_);  
  Client_Sys.Add_To_Attr ('CF$_APP_LINE_NOTES'     , note_text_     ,attr_cf_);

  RETURN_MATERIAL_LINE_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
  COMMIT;  
  
  info_ := 'Collection line note updated';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Update_Col_Line_note;

PROCEDURE Packing_List_Email(info_                 OUT VARCHAR2,
                             shipment_id_list_     IN VARCHAR2,
                             layout_name_          IN VARCHAR2 DEFAULT NULL)                          
IS
 --shipment_id_list_ has to be a comma seperated string like '20,40,180,100'
   printer_id_             VARCHAR2(250);
   print_job_attr_         VARCHAR2(200);
   report_attr_            VARCHAR2(2000);
   parameter_attr_         VARCHAR2(2000);
   result_key_             NUMBER;
   print_job_id_           NUMBER;
   printer_id_list_        VARCHAR2(32000); 
   result_key_string_      VARCHAR2(32000):= NULL;
   
   customer_name_          VARCHAR2(2000);
   company_id_             VARCHAR2(100);
   company_name_           VARCHAR2(100);
   company_email_          VARCHAR2(500);
   cust_ord_no_            VARCHAR2(1000);
   cust_po_no_             VARCHAR2(1000);
   cust_ord_no_string_     VARCHAR2(10000);
   cust_po_no_string_      VARCHAR2(10000);
   address_                VARCHAR2(20000);
   to_email_address_       VARCHAR2(1000);
   count_                  NUMBER;
   
   CURSOR get_individual_shipment IS
     SELECT regexp_substr(shipment_id_list_,'[^,]+', 1, level) shipment_id FROM dual 
     CONNECT BY regexp_substr(shipment_id_list_, '[^,]+', 1, level) IS NOT NULL;
     
   CURSOR get_shipment_count IS
     SELECT count(regexp_substr(shipment_id_list_,'[^,]+', 1, level)) shipment_id FROM dual 
     CONNECT BY regexp_substr(shipment_id_list_, '[^,]+', 1, level) IS NOT NULL;  
     
   CURSOR get_company_email(comp_id_ VARCHAR2) IS
    SELECT t.value 
    FROM COMM_METHOD_TAB t
    WHERE t.party_type = 'COMPANY' 
    AND t.identity = comp_id_
    AND t.name = 'Despatch Email'
    AND t.method_id = 'E_MAIL';  
    
   CURSOR get_co_numbers(shipment_id_ NUMBER) IS 
    select listagg(c.source_ref1, ', ') within group (order by c.source_ref1) from
    (select distinct t.source_ref1
    from SHIPMENT_LINE_TAB t
    where t.shipment_id = shipment_id_
    and t.source_ref_type = 'CUSTOMER_ORDER') c;  
    
  CURSOR get_cust_po_numbers(shipment_id_ NUMBER) IS
    select listagg(customer_po_no, ', ') within group (order by customer_po_no) 
    from  customer_order_tab
    where order_no in (
      select t.source_ref1
      from SHIPMENT_LINE_TAB t
      where t.shipment_id = shipment_id_
      and t.source_ref_type = 'CUSTOMER_ORDER');
      
  CURSOR get_cust_email(cust_id_ VARCHAR2) IS
    SELECT t.value 
    FROM COMM_METHOD_TAB t
    WHERE t.party_type = 'CUSTOMER' 
    AND t.identity = cust_id_
    AND t.name = 'Delivery Note Email'
    AND t.method_id = 'E_MAIL';   

BEGIN

   General_SYS.Init_Method(lu_name_, 'COVALENT_POD_API', 'Packing_List_Email'); 
   
   OPEN get_shipment_count;
   FETCH get_shipment_count INTO count_;
   CLOSE get_shipment_count;
   
   FOR rec_ IN get_individual_shipment LOOP
     
      result_key_  := null;
      cust_ord_no_ := null;
      cust_po_no_  := null;
      
      IF customer_name_ IS NULL THEN
        customer_name_ := Shipment_Source_Utility_API.Get_Receiver_Name(Shipment_Api.Get_Receiver_Id(to_number(rec_.shipment_id)),Shipment_Api.Get_Receiver_Type_Db(to_number(rec_.shipment_id)));
      END IF;  
      
      IF company_email_ IS NULL THEN
        company_id_ := Company_Site_Api.Get_Company(Shipment_Api.Get_Contract(to_number(rec_.shipment_id)));
        company_name_ := Company_Api.Get_Name(company_id_);
        
        OPEN get_company_email(company_id_);
        FETCH get_company_email INTO company_email_;
        CLOSE get_company_email;
      END IF;  
      
      IF to_email_address_ IS NULL THEN
        
        OPEN get_cust_email(Shipment_Api.Get_Receiver_Id(to_number(rec_.shipment_id)));
        FETCH get_cust_email INTO to_email_address_;
        CLOSE get_cust_email;
        
      END IF;
      
      IF address_ IS NULL THEN
        address_ := Shipment_Api.Get_Receiver_Address1(to_number(rec_.shipment_id)) || ', ' ||
                    Shipment_Api.Get_Receiver_Address2(to_number(rec_.shipment_id)) || ', ' ||
                    Shipment_Api.Get_Receiver_City(to_number(rec_.shipment_id)) || ', ' || 
                    Shipment_Api.Get_Receiver_County(to_number(rec_.shipment_id)) || ', ' || 
                    Shipment_Api.Get_Receiver_Zip_Code(to_number(rec_.shipment_id)) || ', ' ||
                    Iso_Country_Api.Get_Description(Shipment_Api.Get_Receiver_Country(to_number(rec_.shipment_id)));
      END IF;  
      
      OPEN get_co_numbers(to_number(rec_.shipment_id));
      FETCH get_co_numbers INTO cust_ord_no_;
      CLOSE get_co_numbers;
      
      OPEN get_cust_po_numbers(to_number(rec_.shipment_id));
      FETCH get_cust_po_numbers INTO cust_po_no_;
      CLOSE get_cust_po_numbers;
      
      IF count_ <= 10 THEN
      
       -- Generate a new print job id
        printer_id_ := Printer_Connection_API.Get_Default_Printer(Fnd_Session_API.Get_Fnd_User, 'SHIPMENT_PACKING_LIST_REP');
        Client_SYS.Clear_Attr(print_job_attr_);
        Client_SYS.Add_To_Attr('PRINTER_ID', printer_id_, print_job_attr_);
        Print_Job_API.New(print_job_id_, print_job_attr_);

        Client_SYS.Clear_Attr(report_attr_);
        Client_SYS.Add_To_Attr('REPORT_ID', 'SHIPMENT_PACKING_LIST_REP', report_attr_);
        Client_SYS.Add_To_Attr('LU_NAME', 'ShipmentLine', report_attr_);

        --Note: Create the report
        Client_SYS.Clear_Attr(parameter_attr_);
        
        client_SYS.Add_To_Attr('SHIPMENT_ID',to_number(rec_.shipment_id),parameter_attr_);
        
        Archive_API.New_Instance(result_key_, report_attr_, parameter_attr_);

        --Note: Connect the created report to a print job id
        Client_SYS.Clear_Attr(print_job_attr_);
        Client_SYS.Add_To_Attr('PRINT_JOB_ID', print_job_id_, print_job_attr_);
        Client_SYS.Add_To_Attr('RESULT_KEY', result_key_, print_job_attr_);
        IF layout_name_ IS NULL THEN
         Client_SYS.Add_To_Attr('LAYOUT_NAME', 'ShipmentPackingListRep.rdl', print_job_attr_);
        ELSE
         Client_SYS.Add_To_Attr('LAYOUT_NAME', layout_name_, print_job_attr_);
        END IF;


        Print_Job_Contents_API.New_Instance(print_job_attr_);

        -- Send the print job to the printer.
        Logical_Printer_API.Enumerate_Printer_Id(printer_id_list_);
        IF (printer_id_list_ IS NOT NULL) THEN
           IF (print_job_id_ IS NOT NULL) THEN
              Print_Job_API.Print(print_job_id_);
           END IF;
        END IF;
        
        Dbms_Output.put_line('Result Key: ' || result_key_ ); 
        
        result_key_string_  := result_key_string_ || ',' || to_char(result_key_);
      
      END IF;
      
      cust_ord_no_string_ := cust_ord_no_string_ || ',' || cust_ord_no_;
      cust_po_no_string_  := cust_po_no_string_ || ',' || cust_po_no_;
      
   END LOOP;  
    
   COMMIT;
   DBMS_LOCK.SLEEP (30); 
   Dbms_Output.put_line(result_key_string_); 
   
   --to_email_address_ := 'Dinusha.deSilva@covalentworld.com'; -- hardcoded for testing
   to_email_address_ := 'Dinusha.deSilva@covalentworld.com;ben.timberlake@covalentworld.com;Isuru.Jayasinghe@covalentworld.com'; -- hardcoded for testing
   
   Send_Email(result_key_string_,customer_name_,company_email_,company_name_,address_,cust_ord_no_string_,cust_po_no_string_,to_email_address_,'SHIPMENT',count_);
  
   Update_Ship_Mail_Sent(info_,shipment_id_list_);

   info_ := 'Shipment Packing List Email Sent';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Packing_List_Email;

PROCEDURE Return_Note_Email(info_                 OUT VARCHAR2,
                            rma_no_list_           IN VARCHAR2,
                            layout_name_           IN VARCHAR2 DEFAULT NULL)                          
IS
 --rma_no_list_ has to be a comma seperated string like '20,40,180,100'
   printer_id_             VARCHAR2(250);
   print_job_attr_         VARCHAR2(200);
   report_attr_            VARCHAR2(2000);
   parameter_attr_         VARCHAR2(2000);
   result_key_             NUMBER;
   print_job_id_           NUMBER;
   printer_id_list_        VARCHAR2(32000); 
   result_key_string_      VARCHAR2(32000):= NULL;
   
   customer_name_          VARCHAR2(2000);
   company_id_             VARCHAR2(100);
   company_name_           VARCHAR2(100);
   company_email_          VARCHAR2(500);
   cust_po_no_             VARCHAR2(1000);
   cust_po_no_string_      VARCHAR2(10000);
   address_                VARCHAR2(20000);
   to_email_address_       VARCHAR2(1000);
   addr1_                  VARCHAR2(500);
   addr2_                  VARCHAR2(500);
   zip_                    VARCHAR2(500);
   city_                   VARCHAR2(500);
   county_                 VARCHAR2(500);
   country_                VARCHAR2(500);
   rma_list_formatted_     VARCHAR2(1000);
   count_                  NUMBER;
   
   CURSOR get_individual_rma IS
     SELECT regexp_substr(rma_no_list_,'[^,]+', 1, level) rma_no FROM dual 
     CONNECT BY regexp_substr(rma_no_list_, '[^,]+', 1, level) IS NOT NULL;
     
   CURSOR get_rma_count IS
     SELECT count(regexp_substr(rma_no_list_,'[^,]+', 1, level)) rma_no FROM dual 
     CONNECT BY regexp_substr(rma_no_list_, '[^,]+', 1, level) IS NOT NULL;  
     
   CURSOR get_company_email(comp_id_ VARCHAR2) IS
    SELECT t.value 
    FROM COMM_METHOD_TAB t
    WHERE t.party_type = 'COMPANY' 
    AND t.identity = comp_id_
    AND t.name = 'Despatch Email'
    AND t.method_id = 'E_MAIL';  
    
   CURSOR get_cust_po_numbers(rma_no_ NUMBER) IS 
    select listagg(c.purchase_order_no, ', ') within group (order by c.purchase_order_no) from
    (select distinct t.purchase_order_no
    from RETURN_MATERIAL_LINE_TAB t
    where t.rma_no = rma_no_) c;  
      
  CURSOR get_cust_email(cust_id_ VARCHAR2) IS
    SELECT t.value 
    FROM COMM_METHOD_TAB t
    WHERE t.party_type = 'CUSTOMER' 
    AND t.identity = cust_id_
    AND t.name = 'Delivery Note Email'
    AND t.method_id = 'E_MAIL';  
    
  CURSOR get_address(rma_no_ NUMBER) IS
    SELECT 
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Address1(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Address1(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Address1(ORDER_NO)) ship_address1,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Address2(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Address2(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Address2(ORDER_NO)) ship_address2,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Zip_Code(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Zip_Code(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Zip_Code(ORDER_NO)) zip_code,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_City(RETURN_FROM_CUSTOMER_NO,
                                                     SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_City(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_City(ORDER_NO)) city,
    DECODE(ORDER_NO,
    NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_County(RETURN_FROM_CUSTOMER_NO,
                                                       SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_County(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_County(ORDER_NO)) county,
    IFSAPP.Iso_Country_API.Get_Description(DECODE(ORDER_NO,
                                             NULL,
     DECODE(SHIPMENT_ID,
            NULL,
            IFSAPP.Customer_Info_Address_API.Get_Country_Code(RETURN_FROM_CUSTOMER_NO,
                                                              SHIP_ADDR_NO),
            IFSAPP.Shipment_API.Get_Receiver_Country(SHIPMENT_ID)),
     IFSAPP.Customer_Order_Address_API.Get_Country_Code(ORDER_NO))) country_
    FROM RETURN_MATERIAL t
    WHERE t.rma_no = rma_no_;   

BEGIN

   General_SYS.Init_Method(lu_name_, 'COVALENT_POD_API', 'Return_Note_Email'); 
   
   OPEN get_rma_count;
   FETCH get_rma_count INTO count_;
   CLOSE get_rma_count;
   
   FOR rec_ IN get_individual_rma LOOP
     
      result_key_  := null;
      cust_po_no_  := null;
      
      IF customer_name_ IS NULL THEN
        customer_name_ := CUST_ORD_CUSTOMER_API.Get_Name(Return_Material_Api.Get_Customer_No(to_number(rec_.rma_no)));
      END IF;  
      
      IF company_email_ IS NULL THEN
        company_id_ := Company_Site_Api.Get_Company(Return_Material_Api.Get_Contract(to_number(rec_.rma_no)));
        company_name_ := Company_Api.Get_Name(company_id_);
        
        OPEN get_company_email(company_id_);
        FETCH get_company_email INTO company_email_;
        CLOSE get_company_email;
      END IF;  
      
      IF to_email_address_ IS NULL THEN
        
        OPEN get_cust_email(Return_Material_Api.Get_Customer_No(to_number(rec_.rma_no)));
        FETCH get_cust_email INTO to_email_address_;
        CLOSE get_cust_email;
        
      END IF;
      
      IF address_ IS NULL THEN
        OPEN get_address(to_number(rec_.rma_no));
        FETCH get_address INTO addr1_,addr2_,zip_,city_,county_,country_;
        CLOSE get_address;
        
        address_ := addr1_ || ', ' ||
                    addr2_ || ', ' ||
                    city_ || ', ' || 
                    county_ || ', ' || 
                    zip_ || ', ' ||
                    country_;
      END IF;  
      
      OPEN get_cust_po_numbers(to_number(rec_.rma_no));
      FETCH get_cust_po_numbers INTO cust_po_no_;
      CLOSE get_cust_po_numbers;
      
      IF count_ <= 10 THEN
      
       -- Generate a new print job id
        printer_id_ := Printer_Connection_API.Get_Default_Printer(Fnd_Session_API.Get_Fnd_User, 'RETURN_MATERIAL_REP');
        Client_SYS.Clear_Attr(print_job_attr_);
        Client_SYS.Add_To_Attr('PRINTER_ID', printer_id_, print_job_attr_);
        Print_Job_API.New(print_job_id_, print_job_attr_);

        Client_SYS.Clear_Attr(report_attr_);
        Client_SYS.Add_To_Attr('REPORT_ID', 'RETURN_MATERIAL_REP', report_attr_);
        Client_SYS.Add_To_Attr('LU_NAME', 'ReturnMaterial', report_attr_);

        --Note: Create the report
        Client_SYS.Clear_Attr(parameter_attr_);
        
        client_SYS.Add_To_Attr('RMA_NO',to_number(rec_.rma_no),parameter_attr_);
        
        Archive_API.New_Instance(result_key_, report_attr_, parameter_attr_);

        --Note: Connect the created report to a print job id
        Client_SYS.Clear_Attr(print_job_attr_);
        Client_SYS.Add_To_Attr('PRINT_JOB_ID', print_job_id_, print_job_attr_);
        Client_SYS.Add_To_Attr('RESULT_KEY', result_key_, print_job_attr_);
        IF layout_name_ IS NULL THEN
         Client_SYS.Add_To_Attr('LAYOUT_NAME', 'ReturnMaterialRep.rdl', print_job_attr_);
        ELSE
         Client_SYS.Add_To_Attr('LAYOUT_NAME', layout_name_, print_job_attr_);
        END IF;


        Print_Job_Contents_API.New_Instance(print_job_attr_);

        -- Send the print job to the printer.
        Logical_Printer_API.Enumerate_Printer_Id(printer_id_list_);
        IF (printer_id_list_ IS NOT NULL) THEN
           IF (print_job_id_ IS NOT NULL) THEN
              Print_Job_API.Print(print_job_id_);
           END IF;
        END IF;
        
        Dbms_Output.put_line('Result Key: ' || result_key_ ); 
        
        result_key_string_  := result_key_string_ || ',' || to_char(result_key_);
      
      END IF;
      
      cust_po_no_string_  := cust_po_no_string_ || ',' || cust_po_no_;
      
   END LOOP;  
    
   COMMIT;
   DBMS_LOCK.SLEEP (30); 
   Dbms_Output.put_line(result_key_string_); 
   
  --to_email_address_ := 'Dinusha.deSilva@covalentworld.com'; -- hardcoded for testing
   to_email_address_ := 'Dinusha.deSilva@covalentworld.com;ben.timberlake@covalentworld.com;Isuru.Jayasinghe@covalentworld.com'; -- hardcoded for testing
   
   rma_list_formatted_ := ','|| rma_no_list_;
   
   Send_Email(result_key_string_,customer_name_,company_email_,company_name_,address_,rma_list_formatted_,cust_po_no_string_,to_email_address_,'RMA',count_);
  
   Update_Col_Mail_Sent(info_,rma_no_list_);

   info_ := 'RMA Email Sent';
  
EXCEPTION WHEN OTHERS THEN
  info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Return_Note_Email;

PROCEDURE Update_Col_Mail_Sent(info_              OUT VARCHAR2,
                               rma_no_list_        IN VARCHAR2) IS
                               
  objid_           VARCHAR2(32000);
  attr_cf_         VARCHAR2(32000);
  attr_            VARCHAR2(32000);
  
  CURSOR get_individual_rma IS
     SELECT regexp_substr(rma_no_list_,'[^,]+', 1, level) rma_no FROM dual 
     CONNECT BY regexp_substr(rma_no_list_, '[^,]+', 1, level) IS NOT NULL;
    
  CURSOR get_objid(rma_no_ NUMBER) IS
    SELECT objid
    FROM RETURN_MATERIAL
    WHERE rma_no = rma_no_;                             
BEGIN
 General_SYS.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Col_Mail_Sent');
 
 FOR rec_ IN get_individual_rma LOOP
   
    objid_ := NULL;

    OPEN get_objid(to_number(rec_.rma_no));
    FETCH get_objid INTO objid_;
    CLOSE get_objid;
    
    Client_Sys.Clear_Attr(attr_cf_);
    Client_Sys.Clear_Attr(attr_);
   
    Client_Sys.Add_To_Attr ('CF$_DESPATCH_EMAIL_SENT_DB'     , 'YES'             ,attr_cf_);

    RETURN_MATERIAL_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
 END LOOP; 
  
 COMMIT;  
  
 info_ := 'No Errors Found';
  
EXCEPTION WHEN OTHERS THEN
 info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
  
END Update_Col_Mail_Sent;    

PROCEDURE Update_Ship_Mail_Sent(info_                 OUT VARCHAR2,
                                shipment_id_list_      IN VARCHAR2) IS
  objid_           VARCHAR2(32000);
  attr_cf_         VARCHAR2(32000);
  attr_            VARCHAR2(32000);       
  
  CURSOR get_individual_shipment IS
     SELECT regexp_substr(shipment_id_list_,'[^,]+', 1, level) shipment_id FROM dual 
     CONNECT BY regexp_substr(shipment_id_list_, '[^,]+', 1, level) IS NOT NULL;    
     
  CURSOR get_objid(shipment_id_ NUMBER) IS
    SELECT objid
    FROM SHIPMENT
    WHERE shipment_id = shipment_id_;
                                                     
BEGIN
 General_SYS.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_Ship_Mail_Sent'); 
 
  FOR rec_ IN get_individual_shipment LOOP
   
    objid_ := NULL;

    OPEN get_objid(to_number(rec_.shipment_id));
    FETCH get_objid INTO objid_;
    CLOSE get_objid;
    
    Client_Sys.Clear_Attr(attr_cf_);
    Client_Sys.Clear_Attr(attr_);
   
    Client_Sys.Add_To_Attr ('CF$_DESPATCH_EMAIL_SENT_DB'     , 'YES'             ,attr_cf_);

    SHIPMENT_CFP.Cf_Modify__(info_ , objid_ , attr_cf_ , attr_ , 'DO' );
  
 END LOOP; 
  
 COMMIT;  
  
 info_ := 'No Errors Found';
  
EXCEPTION WHEN OTHERS THEN
 info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
END Update_Ship_Mail_Sent;

PROCEDURE Get_Next_Stop_Shipments (shipment_       OUT CLOB,
                                   email_address_   IN VARCHAR2) IS
  
 json_shipment_             pljson;
 json_single_shipment_      pljson;
 json_ar_shipment_          pljson_list;
 json_                      CLOB;
 
 CURSOR get_shipment_info IS
   SELECT t.cf$_transport_id, t.cf$_order, t.cf$_source_id, t.cf$_date_time
   FROM TRANSPORT_SCHEDULING_CLV t, shipment_cfv r
   WHERE t.cf$_source_id = r.shipment_id 
   AND t.CF$_SOURCE = 'SHIPMENT'
   AND t.CF$_APP_STATUS = 'Not Processed'
   AND SUBSTR(r.cf$_Delivery_App_User, 1, INSTR(r.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                         FROM fnd_user_property_tab t
                                                                                         WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                         AND t.value = email_address_);
 
  
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Get_Next_Stop_Shipments');
  
  json_shipment_            := pljson();
  json_single_shipment_     := pljson();

  json_ar_shipment_         :=  pljson_list();
  
  
  FOR shipment_rec_ IN get_shipment_info LOOP
    
    json_single_shipment_.put ('TransportId'            , shipment_rec_.cf$_transport_id);
    json_single_shipment_.put ('SortOrder'              , shipment_rec_.cf$_order);
    json_single_shipment_.put ('ShipmentId'             , shipment_rec_.cf$_source_id);
    json_single_shipment_.put ('DateTime'               , TO_CHAR((shipment_rec_.cf$_date_time), 'DD/MM/YYYY HH24:MI:SS'));
    
    json_ar_shipment_.append (json_single_shipment_.to_json_value);
  
  END LOOP;
  

  json_shipment_.put ('Shipments', json_ar_shipment_);
  
  json_shipment_.print;
  
  
  dbms_lob.createtemporary(json_, true);
  json_shipment_.to_clob(json_, false, dbms_lob.lobmaxsize);

 shipment_ := json_;

 EXCEPTION WHEN OTHERS THEN
  shipment_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);

  
END Get_Next_Stop_Shipments;

PROCEDURE Get_Next_Stop_Collections(rma_             OUT CLOB,
                                    email_address_   IN  VARCHAR2) IS
  
 json_collection_             pljson;
 json_single_collection_      pljson;
 json_ar_collection_          pljson_list;
 json_                      CLOB;
 
 CURSOR get_collection_info IS
   SELECT t.cf$_transport_id, t.cf$_order, t.cf$_source_id, t.cf$_date_time
   FROM TRANSPORT_SCHEDULING_CLV t, RETURN_MATERIAL_CFV r
   WHERE t.cf$_source_id = r.RMA_NO 
   AND t.CF$_SOURCE = 'RMA'
   AND t.CF$_APP_STATUS = 'Not Processed'
   AND SUBSTR(r.cf$_Delivery_App_User, 1, INSTR(r.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                         FROM fnd_user_property_tab t
                                                                                         WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                         AND t.value = email_address_);
 
  
BEGIN
  General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Get_Next_Stop_Collections');
  
  json_collection_            := pljson();
  json_single_collection_     := pljson();

  json_ar_collection_         :=  pljson_list();
  
  
  FOR collection_rec_ IN get_collection_info LOOP
    
    json_single_collection_.put ('TransportId'            , collection_rec_.cf$_transport_id);
    json_single_collection_.put ('SortOrder'              , collection_rec_.cf$_order);
    json_single_collection_.put ('CollectionId'           , collection_rec_.cf$_source_id);
    json_single_collection_.put ('DateTime'               , TO_CHAR((collection_rec_.cf$_date_time), 'DD/MM/YYYY HH24:MI:SS'));
    
    json_ar_collection_.append (json_single_collection_.to_json_value);
  
  END LOOP;
  

  json_collection_.put ('Collections', json_ar_collection_);
  
  json_collection_.print;
  
  
  dbms_lob.createtemporary(json_, true);
  json_collection_.to_clob(json_, false, dbms_lob.lobmaxsize);

  rma_ := json_;

 EXCEPTION WHEN OTHERS THEN
  rma_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);

  
END Get_Next_Stop_Collections;

PROCEDURE Get_Next_Stop_Items(item_             OUT CLOB,
                              email_address_    IN  VARCHAR2) IS
                              
 json_single_collection_      pljson;
 json_single_shipment_        pljson;
 json_item_                   pljson;
 json_ar_item_                pljson_list;
 json_                        CLOB;
 
  CURSOR get_shipment_info IS
   SELECT t.cf$_transport_id, t.cf$_order, t.cf$_source_id, t.cf$_date_time
   FROM TRANSPORT_SCHEDULING_CLV t, shipment_cfv r
   WHERE t.cf$_source_id = r.shipment_id 
   AND t.CF$_SOURCE = 'SHIPMENT'
   AND t.CF$_APP_STATUS = 'Not Processed'
   AND SUBSTR(r.cf$_Delivery_App_User, 1, INSTR(r.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                         FROM fnd_user_property_tab t
                                                                                         WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                         AND t.value = email_address_);
 
 
 CURSOR get_collection_info IS
   SELECT t.cf$_transport_id, t.cf$_order, t.cf$_source_id, t.cf$_date_time
   FROM TRANSPORT_SCHEDULING_CLV t, RETURN_MATERIAL_CFV r
   WHERE t.cf$_source_id = r.RMA_NO 
   AND t.CF$_SOURCE = 'RMA'
   AND t.CF$_APP_STATUS = 'Not Processed'
   AND SUBSTR(r.cf$_Delivery_App_User, 1, INSTR(r.cf$_Delivery_App_User, '-') -2) IN (SELECT t.identity 
                                                                                         FROM fnd_user_property_tab t
                                                                                         WHERE t.name = 'SMTP_MAIL_ADDRESS'
                                                                                         AND t.value = email_address_);
 
                               
                              
BEGIN
 General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Get_Next_Stop_Items');
 
  json_single_collection_   := pljson();
  json_single_shipment_     := pljson();
  json_item_                := pljson();
  json_ar_item_             := pljson_list();
  
  FOR shipment_rec_ IN get_shipment_info LOOP
    
    json_single_shipment_.put ('TransportId'            , shipment_rec_.cf$_transport_id);
    json_single_shipment_.put ('SortOrder'              , shipment_rec_.cf$_order);
    json_single_shipment_.put ('Source'                 , 'Shipment');
    json_single_shipment_.put ('ItemId'                 , shipment_rec_.cf$_source_id);
    json_single_shipment_.put ('CustomerName'           , Get_Customer_Name('SHIPMENT',shipment_rec_.cf$_source_id));
    json_single_shipment_.put ('DateTime'               , TO_CHAR((shipment_rec_.cf$_date_time), 'DD/MM/YYYY HH24:MI:SS'));
    
    json_ar_item_.append (json_single_shipment_.to_json_value);
  
  END LOOP;
  
  FOR collection_rec_ IN get_collection_info LOOP
    
    json_single_collection_.put ('TransportId'            , collection_rec_.cf$_transport_id);
    json_single_collection_.put ('SortOrder'              , collection_rec_.cf$_order);
    json_single_collection_.put ('Source'                 , 'Collection');
    json_single_collection_.put ('ItemId'                 , collection_rec_.cf$_source_id);
    json_single_collection_.put ('CustomerName'           , Get_Customer_Name('RMA',collection_rec_.cf$_source_id));
    json_single_collection_.put ('DateTime'               , TO_CHAR((collection_rec_.cf$_date_time), 'DD/MM/YYYY HH24:MI:SS'));
    
    json_ar_item_.append (json_single_collection_.to_json_value);
  
  END LOOP;
  

  json_item_.put ('Items', json_ar_item_);
  
  json_item_.print;
  
  
  dbms_lob.createtemporary(json_, true);
  json_item_.to_clob(json_, false, dbms_lob.lobmaxsize);

  item_ := json_;

 EXCEPTION WHEN OTHERS THEN
  item_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000);
  
END Get_Next_Stop_Items;                              

PROCEDURE Update_txn_status (info_             OUT VARCHAR2,
                             source_           IN  VARCHAR2, 
                             source_id_        IN  NUMBER,                  
                             transport_id_     IN  VARCHAR2,
                             status_           IN  VARCHAR2) IS
                             
  p0_                VARCHAR2(32000) := NULL;
  objid_             VARCHAR2(1000);
  objversion_        VARCHAR2(1000);
  attr_              VARCHAR2(32000);                            
                             
 CURSOR get_object IS
  SELECT t.objid, t.objversion 
  FROM TRANSPORT_SCHEDULING_CLV t
  WHERE t.CF$_SOURCE = source_
  AND t.CF$_SOURCE_ID = source_id_
  AND t.cf$_transport_id = transport_id_;                            
                             
BEGIN
General_Sys.Init_Method(lu_name_, 'COVALENT_POD_API', 'Update_txn_status');  

  OPEN get_object;
  FETCH get_object INTO objid_,objversion_;
  CLOSE get_object;
  
  Client_sys.Clear_Attr(attr_);  
  attr_ := 'CF$_APP_STATUS'||chr(31)|| status_ ||chr(30);
      
  TRANSPORT_SCHEDULING_CLP.MODIFY__( p0_ , objid_ , objversion_ , attr_ , 'DO' );
  
 COMMIT;
 
 info_ := 'Transport status updated';
EXCEPTION WHEN OTHERS THEN
 info_ := SUBSTR((SQLCODE|| '\'|| SQLERRM), 1, 2000); 
END Update_txn_status; 

FUNCTION Get_Planned_Ship_Date(source_         IN  VARCHAR2,
                               source_id_      IN  NUMBER)RETURN DATE 
 IS
 
 planned_ship_date_    DATE;

BEGIN
 IF source_ = 'SHIPMENT' THEN
   planned_ship_date_ := Shipment_Api.Get_Planned_Ship_Date(source_id_);
 ELSIF source_ = 'RMA' THEN
   planned_ship_date_ := Return_Material_Cfp.Get_Cf$_Plan_Ship_Date(Return_Material_Cfp.Get_Objkey(source_id_));
 END IF; 
 
 RETURN planned_ship_date_;
 
END Get_Planned_Ship_Date;      

FUNCTION Get_Customer_Name (source_         IN  VARCHAR2,
                            source_id_      IN  NUMBER) RETURN VARCHAR2 
 IS
  cust_name_            VARCHAR2(1000);
BEGIN
 IF source_ = 'SHIPMENT' THEN
   cust_name_ := Shipment_Source_Utility_API.Get_Receiver_Name(Shipment_Api.Get_Receiver_Id(source_id_),Shipment_Api.Get_Receiver_Type_Db(source_id_));
 ELSIF source_ = 'RMA' THEN
   cust_name_ := CUST_ORD_CUSTOMER_API.Get_Name(Return_Material_Api.Get_Customer_No(source_id_));
 END IF; 
  
 RETURN cust_name_;
  
END Get_Customer_Name;  

FUNCTION Get_Address (source_         IN  VARCHAR2,
                      source_id_      IN  NUMBER) RETURN VARCHAR2
 IS
 address_                VARCHAR2(20000);
 addr1_                  VARCHAR2(500);
 addr2_                  VARCHAR2(500);
 zip_                    VARCHAR2(500);
 city_                   VARCHAR2(500);
 county_                 VARCHAR2(500);
 country_                VARCHAR2(500);
 
 CURSOR get_address(rma_no_ NUMBER) IS
    SELECT 
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Address1(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Address1(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Address1(ORDER_NO)) ship_address1,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Address2(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Address2(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Address2(ORDER_NO)) ship_address2,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Zip_Code(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Zip_Code(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Zip_Code(ORDER_NO)) zip_code,
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_City(RETURN_FROM_CUSTOMER_NO,
                                                     SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_City(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_City(ORDER_NO)) city,
    DECODE(ORDER_NO,
    NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_County(RETURN_FROM_CUSTOMER_NO,
                                                       SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_County(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_County(ORDER_NO)) county,
    IFSAPP.Iso_Country_API.Get_Description(DECODE(ORDER_NO,
                                             NULL,
     DECODE(SHIPMENT_ID,
            NULL,
            IFSAPP.Customer_Info_Address_API.Get_Country_Code(RETURN_FROM_CUSTOMER_NO,
                                                              SHIP_ADDR_NO),
            IFSAPP.Shipment_API.Get_Receiver_Country(SHIPMENT_ID)),
     IFSAPP.Customer_Order_Address_API.Get_Country_Code(ORDER_NO))) country_
    FROM RETURN_MATERIAL t
    WHERE t.rma_no = source_id_;
BEGIN
 IF source_ = 'SHIPMENT' THEN
    address_ := Shipment_Api.Get_Receiver_Address1(source_id_) || ', ' ||
                Shipment_Api.Get_Receiver_Address2(source_id_) || ', ' ||
                Shipment_Api.Get_Receiver_City(source_id_) || ', ' || 
                Shipment_Api.Get_Receiver_County(source_id_) || ', ' || 
                Shipment_Api.Get_Receiver_Zip_Code(source_id_) || ', ' ||
                Iso_Country_Api.Get_Description(Shipment_Api.Get_Receiver_Country(source_id_));
    
 ELSIF source_ = 'RMA' THEN
   
  OPEN get_address(source_id_);
  FETCH get_address INTO addr1_,addr2_,zip_,city_,county_,country_;
  CLOSE get_address;
        
  address_ := addr1_ || ', ' ||
              addr2_ || ', ' ||
              city_ || ', ' || 
              county_ || ', ' || 
              zip_ || ', ' ||
              country_;
 END IF;
 
 RETURN address_; 
END Get_Address; 

FUNCTION Get_Zip_Code (source_         IN  VARCHAR2,
                       source_id_      IN  NUMBER) RETURN VARCHAR2
IS

zip_code_     VARCHAR2(100);

CURSOR get_address IS
    SELECT 
    DECODE(ORDER_NO,
           NULL,
    DECODE(SHIPMENT_ID,
           NULL,
           IFSAPP.Customer_Info_Address_API.Get_Zip_Code(RETURN_FROM_CUSTOMER_NO,
                                                         SHIP_ADDR_NO),
           IFSAPP.Shipment_API.Get_Receiver_Zip_Code(SHIPMENT_ID)),
    IFSAPP.Customer_Order_Address_API.Get_Zip_Code(ORDER_NO)) zip_code   
    FROM RETURN_MATERIAL t
    WHERE t.rma_no = source_id_;
BEGIN
 IF source_ = 'SHIPMENT' THEN
   
   zip_code_ := Shipment_Api.Get_Receiver_Zip_Code(source_id_);
    
 ELSIF source_ = 'RMA' THEN
   
  OPEN get_address;
  FETCH get_address INTO zip_code_;
  CLOSE get_address;
  
 END IF;
  
 RETURN zip_code_;
 
END Get_Zip_Code;                        
                                                                                                                            
-----------------------------------------------------------------------------
-------------------- FOUNDATION1 METHODS ------------------------------------
-----------------------------------------------------------------------------
-- Init
--   Dummy procedure that can be called at database startup to ensure that
--   this package is loaded into memory for performance reasons only.
-----------------------------------------------------------------------------

PROCEDURE Init
IS
BEGIN
   NULL;
END Init;


END COVALENT_POD_API;
/


prompt Done
spool off
set define on
